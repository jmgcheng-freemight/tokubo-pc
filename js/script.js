$(document).ready(function(){

	$(".bgcontent.bgcontent-sweets").vegas({
	    slides: [
	        { src: "img/sweet-bg-5.png" },
	        { src: "img/sweet-bg-6.png" }
	    ]
	});

	$(".lazy").lazyload({
		effect : "fadeIn",
		effect_speed : 3000
	});

	$(".slider-slick").slick({
		dots: true,
		infinite: true,
		/*centerMode: true,
		variableWidth: true,*/
		autoplay: true,
		fade: true,
		cssEase: 'linear',
  		adaptiveHeight: true,
  		arrows: false
	});


	/*
		lazy-load
	*/
	var el = $("<div>");
	$.support.transform  = typeof el.css("transform") === "string";
    $.support.transition = typeof el.css("transitionProperty") === "string";
    if($.support.transform && $.support.transition) {
		$(window).on("load scroll", function(){
			var i_screen = $(window).scrollTop() + $(window).height();
			$('.lazy-load').each(function(index, e){
				if( i_screen > ( $(this).offset().top + $(this).height() ) && !$(this).hasClass('lazy-load-show')){
					$(this).addClass('lazy-load-show');
				}
			});
		});
    }
    else
    {
    	$('.lazy-load').addClass('lazy-load-show');
    }





});
