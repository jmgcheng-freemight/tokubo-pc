# Tokubo - PC

## Tasks To Do:
* header - base /
* header - menu base /
* top page - bg slider /
* top page - img side contents /
* top page - img side contents buttons /
* top page - img side contents bottom margins /
* footer - base /
* top page - img side contents - lacking images /
* header - menu - cart icon /
* header - menu - bar menu /
* update glyphter /
* seperate header and footer template /
* products page - slider /
* top page - img side contents show when scrolled animation /
* products page - product list /
* products page - back button /
* header - remove bar menu /
* header - brand - lazy loading /
* top page - img side content - more lazy loading /
* js - create own lazy loading base freemight js /
* top page - img side content - button lazy loading /
* top page - expand main width /
* product page - portrait /
* product page - desc /
* product page - form /
* product page - gallery /
* product page - control /
* product page - misc contents - static /
* product page - misc contents - dynamic data /
* contact us
* company profile
* -