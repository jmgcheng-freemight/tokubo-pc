﻿<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/vegas.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/slick.css" type="text/css" />
		<link rel="stylesheet" href="css/slick-theme.css" type="text/css" />
		<link rel="stylesheet" href="css/Glyphter.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<title>Tokubo</title>
	</head>
	<body>
		<header class="l-header">
			<div class="l-content">
				<div class="l-gutter">

					<div class="header-brand">
						<!-- <span class="header-brand-introtext">
							料亭のおもてなしおやつ
						</span> -->
						<h1 class="header-brand-h">
							<a href="index.php">
								<img class="lazy-load" src="img/brand.jpg" />
							</a>
						</h1>
					</div>
					<nav class="nav nav-header ">
						<ul>
							<li>
								<a href="products.php" >
									商品購入<span class="cart"><i class="icon-cart"><span class="cart-counter">7</span></i></span>
								</a> 
							</li>
							<li>
								<a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i> 公式Facebook</a>
							</li>
							<li>
								<a href="#">西乃川公式HP</a>
							</li>
							<li>
								<a href="#">お問い合せ</a>
							</li>
							<li>
								<a href="#">会社概要</a>
							</li>
						</ul>
					</nav>

				</div>
			</div>
		</header>

