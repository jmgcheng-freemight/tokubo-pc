﻿<?php 
	include 'header.php';

		/* these should be in db.. */
		$a_product_details = array();
		if( isset($_GET['id']) && !empty($_GET['id']) ) {
			$a_product_details['i_id'] = $_GET['id'];
		}
		else {
			$a_product_details['i_id'] = 1;	
		}
		if( $a_product_details['i_id'] == 1 ) {
			$a_product_details['s_img_portrait'] = 'img/img-8.jpg';
			$a_product_details['s_product_desc'] = 'img/product-desc-1.jpg';
			$a_product_details['s_product_name'] = 'とくぼう チーズ10本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-11.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-12.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-13.jpg" );
		}
		elseif ( $a_product_details['i_id'] == 2 ) {
			$a_product_details['s_img_portrait'] = 'img/img-9.jpg';
			$a_product_details['s_product_desc'] = 'img/product-desc-2.jpg';
			$a_product_details['s_product_name'] = 'とくぼう あんこ10本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-14.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-15.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-13.jpg" );
		}
		elseif ( $a_product_details['i_id'] == 3 ) {
			$a_product_details['s_img_portrait'] = 'img/img-10.jpg';
			$a_product_details['s_product_desc'] = 'img/product-desc-3.jpg';
			$a_product_details['s_product_name'] = 'とくぼう チーズ5本 あんこ5本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-11.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-14.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-13.jpg" );
		}
		else {
			header("Location: products.php");
			die();
		}


?>

		<main>

			<div class="l-content">
				<div class="l-gutter">

					<div class="product-brochure">

						<?php if( isset($a_product_details['s_img_portrait']) && !empty($a_product_details['s_img_portrait']) ) : ?>
						<div class="product-brochure-portrait">
							<img src="<?php echo $a_product_details['s_img_portrait']; ?>" />
						</div>
						<?php endif; ?>

						<div class="l-table width-full">

							<div class="l-table-cell align-left" >
								<?php if( isset($a_product_details['s_product_desc']) && !empty($a_product_details['s_product_desc']) ) : ?>
								<div class="product-brochure-desc">
									<img src="<?php echo $a_product_details['s_product_desc']; ?>" />
								</div>
								<?php endif; ?>
							</div>
							
							<div class="l-table-cell align-right" >
								<div class="product-brochure-cp">
									<form>
										<?php if( isset($a_product_details['s_product_name']) && !empty($a_product_details['s_product_name']) ) : ?>
										<h5 class="product-name">
											<?php echo $a_product_details['s_product_name']; ?>
										</h5>
										<?php endif; ?>
										<div class="l-table width-full">
											<div class="l-table-row">
												<div class="l-table-cell product-brochure-cp-label">
													<label>
														数量
													</label>
												</div>
												<div class="l-table-cell product-brochure-cp-detail">
													<input type="text" value="" />
												</div>
											</div>
											<div class="l-table-row">
												<div class="l-table-cell product-brochure-cp-label">
													<label>
														箱
													</label>
												</div>
												<div class="l-table-cell product-brochure-cp-detail">
													<select>
														<option>選択してください</option>
													</select>
												</div>
											</div>
											<div class="l-table-row">
												<div class="l-table-cell product-brochure-cp-label">
													<label>
														価格
													</label>
												</div>
												<div class="l-table-cell product-brochure-cp-detail">
													<p>
														¥1,600 <span>（税込）</span>
													</p>
												</div>
											</div>
										</div>
										<div class="product-brochure-cp-control">
											<a href="#" class="button button-yellow width-full">
												<!-- <span class="button-ico button-ico-left"></span> -->
												<i class="icon-cart"></i> &nbsp;&nbsp; 買い物かごに入れる
											</a>
										</div>
									</form>
								</div>
							</div>

						</div>

						<?php 
							if( isset($a_product_details['a_product_gallery']) && !empty($a_product_details['a_product_gallery']) ):
						?>
						<div class="product-brochure-gallery">
							<ul class="l-grid l-grid-3cols">
							<?php 
								foreach( $a_product_details['a_product_gallery'] AS $s_product_gallery ):
							?>
								<li class="l-grid-col">
									<div class="product-brochure-gallery-portrait">
										<div class="product-brochure-gallery-portrait-inner">
											<img src="<?php echo $s_product_gallery; ?>" />
										</div>
									</div>
								</li>
							<?php 
								endforeach;
							?>
							</ul>
						</div>
						<?php 
							endif;
						?>


						<div class="pagecontrol align-center">
							<a href="#" class="button button-red button-cornerround button-fixedwidth button-fixedwidth-large">
								<span class="button-ico button-ico-left">
									<i class="icon-triangle-left"></i>
								</span>
								戻 る
							</a>
							<a href="#" class="button button-yellow button-cornerround button-fixedwidth button-fixedwidth-large">
								<!-- <span class="button-ico button-ico-left"></span> -->
								<i class="icon-cart"></i> &nbsp;&nbsp; 買い物かごに入れる
							</a>
						</div>


						<div class="product-brochure-misccontent">
							<img src="img/product-misc-content-1.jpg" />

							<div class="l-table width-full">
								<div class="l-table-cell align-left" >
									<img src="img/product-misc-content-2.jpg" />
								</div>
								
								<div class="l-table-cell align-right" >
									<div class="product-brochure-misccontent-modimg">
										<a href="#" class="button button-red button-cornerround">
											西乃川公式HPへ &nbsp;<i class="icon-triangle-right"></i>
										</a>
										<img src="img/product-misc-content-3.jpg" />
									</div>
								</div>
							</div>

							<img src="img/product-misc-content-4.jpg" />
							<img src="img/product-misc-content-5.jpg" />
							<img src="img/product-misc-content-6.jpg" />
							<img src="img/product-misc-content-7.jpg" />

						</div>


						<div class="pagecontrol align-center">
							<a href="#" class="button button-red button-cornerround button-fixedwidth button-fixedwidth-large">
								<span class="button-ico button-ico-left">
									<i class="icon-triangle-left"></i>
								</span>
								戻 る
							</a>
							<a href="#" class="button button-yellow button-cornerround button-fixedwidth button-fixedwidth-large">
								<!-- <span class="button-ico button-ico-left"></span> -->
								<i class="icon-cart"></i> &nbsp;&nbsp; 買い物かごに入れる
							</a>
						</div>

					</div>

				</div>
			</div>

		</main>
	

<?php 
	include 'footer.php';
?>