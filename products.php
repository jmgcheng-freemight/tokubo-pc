﻿<?php 
	include 'header.php';
?>

		<main>

			<div class="l-content">
				<div class="l-gutter">

					<div class="slider slider-products slider-slick">
						<div>
							<img src="img/img-1.jpg" />
						</div>
						<div>
							<img src="img/img-16.jpg" />
						</div>
						<div>
							<img src="img/img-17.jpg" />
						</div>
						<div>
							<img src="img/img-18.jpg" />
						</div>
						<div>
							<img src="img/img-19.jpg" />
						</div>
					</div>


					<div class="product-cards">
						<ul class="l-grid l-grid-3cols">
							<li class="l-grid-col">
								<a class="product-cards-anc" href="product.php?id=1">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/img-5.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h4 class="product-cards-name">とくぼう チーズ10本</h4>
										<span class="product-cards-price">1,600円</span>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="product.php?id=2">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/img-6.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h4 class="product-cards-name">とくぼう あんこ10本</h4>
										<span class="product-cards-price">1,600円</span>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="product.php?id=3">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/img-7.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h4 class="product-cards-name">とくぼう チーズ5本　あんこ5本</h4>
										<span class="product-cards-price">1,600円</span>
									</div>
								</a>
							</li>
						</ul>
					</div>


					<div class="pagecontrol align-center">
						<a href="#" class="button button-red button-cornerround button-fixedwidth button-fixedwidth-medium">
							<span class="button-ico button-ico-left">
								<i class="icon-triangle-left"></i>
							</span>
							戻 る
						</a>
					</div>

				</div>
			</div>

		</main>
	

<?php 
	include 'footer.php';
?>