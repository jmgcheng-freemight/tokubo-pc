﻿<?php 
	include 'header.php';
?>

		<main>

			<div class="bgcontent bgcontent-sweets" > <!-- h1385 -->
				<div class="l-content l-content-width-medium">
					<div class="l-gutter">
						<div class="l-grid l-grid-3cols">
							<div class="l-grid-col">
								<div class="sidecontent sidecontent-left">
									<img class="lazy-load" src="img/top-sidecontent-left-1.jpg" />
									<img class="lazy-load" src="img/top-sidecontent-left-2.jpg" />
									<img class="lazy-load" src="img/top-sidecontent-left-3.jpg" />
									<div class="">
										<a href="#" class="lazy-load button button-red button-cornerround">
											とくぼうを購入する &nbsp;<i class="icon-triangle-right"></i>
											<!-- とくぼうを購入する &nbsp;<i class="triangle triangle-right"></i> -->
											<!-- <span class="button-ico button-ico-right"></span> -->
										</a>
									</div>
								</div>
							</div>
							<div class="l-grid-col">
							</div>
							<div class="l-grid-col">
								<div class="sidecontent sidecontent-right">
									<img class="lazy-load" src="img/top-sidecontent-right-1.jpg" />
									<div class="">
										<a href="#" class="lazy-load button button-red button-cornerround">
											西乃川公式HPへ &nbsp;<i class="icon-triangle-right"></i>
										</a>
									</div>
									<img class="lazy-load" src="img/top-sidecontent-right-2.jpg" />
									<img class="lazy-load" src="img/top-sidecontent-right-3.jpg" />
									<img class="lazy-load" src="img/top-sidecontent-right-4.jpg" />
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>
	

<?php 
	include 'footer.php';
?>